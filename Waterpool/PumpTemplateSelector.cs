﻿using System.Windows;
using System.Windows.Controls;

namespace Waterpool
{
    public sealed class PumpTemplateSelector : DataTemplateSelector
    {
        public DataTemplate PumpIn { get; set; }
        public DataTemplate PumpOut { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if ((item as Pump).PumpForce >= 0) return PumpIn;
            else return PumpOut;
        }
    }
}
