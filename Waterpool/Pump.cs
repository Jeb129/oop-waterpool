﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading;

namespace Waterpool
{
    public class Pump : INotifyPropertyChanged
    {
        static Mutex WorkFree = new();
        public int PumpForce { get; set; }
        bool _DoPump;
        bool _IsOn = true;
        MainWindow _PoolWindow;
        public Pump(int pumpForce)
        {
            PumpForce = pumpForce;
            if (PumpForce < 0)
            {
                MainWindow.PoolOverflov += StartPump;
                MainWindow.PoolEmpty += StopPump;
            }
        }
        public bool IsOn
        {
            get { return _IsOn; }
            set
            {
                if (_IsOn != value)
                {
                    _IsOn = value;
                    if (!_IsOn) StopPump(_PoolWindow);
                    OnPropertyChanged(nameof(IsOn));
                }
            }
        }

        public void StartPump(object s)
        {
            if (!IsOn) return;
            _PoolWindow = s as MainWindow;
            _DoPump = true;
            (new Thread(Pumping)).Start();
        }
        public void StopPump(object s)
        {
            _DoPump = false;
        }
        void Pumping()
        {
            while (_DoPump)
            {
                WorkFree.WaitOne();
                _PoolWindow.WaterLevel += PumpForce;
                WorkFree.ReleaseMutex();
                int cd = 1000 / (MainWindow.UPS * MainWindow.SimulationSpeed);
                Thread.Sleep(cd);
            }
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
