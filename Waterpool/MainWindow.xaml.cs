﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows;
using System.Windows.Controls;

namespace Waterpool
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public MainWindow()
        {
            DataContext = this;
            InitializeComponent();
        }
        public delegate void PoolEventHandler(object sender);

        public static event PoolEventHandler? PoolOverflov;
        public static event PoolEventHandler? PoolEmpty;

        #region Настройки
        static byte _UPS = 100; // Обновления в секунду
        static byte _SimulationSpeed = 1; // Ускорение симуляции
        int _WaterLevel = 200 * _UPS; // Начальное количество жидкости
        int _PoolCapacity = 2000 * _UPS; // Максимальное количество жидкости
        int _VisualLowBorder = 500; // Нижняя граница уровня жидкости
        int _VisualHighBorder = 1500; // Верхняя граница уровня жидкости
        static int _FillingForce = 50; // Начальная скорость закачки
        #endregion
        #region Свойства
        #region Общие ресурсы
        public static byte UPS
        {
            get { return _UPS; }
        }
        public static byte SimulationSpeed
        {
            get { return _SimulationSpeed; }
        }
        public int WaterLevel
        {
            get { return _WaterLevel; }
            set
            {
                if (_WaterLevel != value)
                {
                    _WaterLevel = Math.Min(value, _PoolCapacity);
                    OnPropertyChanged(nameof(VisualWaterLevel));

                    if (VisualWaterLevel > _VisualHighBorder && !NeedPump)
                    {
                        PoolOverflov?.Invoke(this);
                        NeedPump = true;
                    }
                    else if (VisualWaterLevel < _VisualLowBorder && NeedPump)
                    {
                        PoolEmpty?.Invoke(this);
                        NeedPump = false;
                    }
                }
            }
        }
        #endregion
        public ObservableCollection<Pump> Pumps
        {
            get => _Pumps;
        }
        public int VisualWaterLevel
        {
            get { return _WaterLevel / _UPS; }
            set { }
        }
        public int VisualPoolCapacity
        {
            get { return _PoolCapacity / _UPS; }
            set { }
        }
        public int VisualFillingForce
        {
            get { return _FillingForce; }
            set
            {
                if (_FillingForce != value)
                {
                    _FillingForce = value;
                    foreach (var pump in _Pumps.Where(pump => pump.PumpForce >= 0))
                        pump.PumpForce = value;
                    OnPropertyChanged(nameof(VisualFillingForce));
                }
            }
        }
        public byte VisualSimulationSpeed
        {
            get { return _SimulationSpeed; }
            set
            {
                if (_SimulationSpeed != value)
                {
                    _SimulationSpeed = value;
                    OnPropertyChanged(nameof(VisualSimulationSpeed));
                }
            }
        }

        bool _NeedPump;
        public bool NeedPump
        {
            get { return _NeedPump; }
            set
            {
                if (_NeedPump != value)
                {
                    _NeedPump = value;
                    OnPropertyChanged(nameof(NeedPump));
                }
            }
        }

        bool _IsWorking;
        public bool IsWorking
        {
            get { return _IsWorking; }
            set
            {
                if (_IsWorking != value)
                {
                    _IsWorking = value;
                    OnPropertyChanged(nameof(IsWorking));
                }
            }
        }
        #endregion

        ObservableCollection<Pump> _Pumps = new()
        {
            new(_FillingForce), // закачивание
            new(-20),new(-20),new(-20),new(-20),new(-20)
        };
        private void PumpOnOff_Click(object sender, RoutedEventArgs e)
        {
            Pump t = (sender as Button).Tag as Pump;
            t.IsOn = !t.IsOn;
            if (IsWorking && t.IsOn)
            {
                if (t.PumpForce < 0)
                {
                    if (NeedPump)
                        t.StartPump(this);
                }
                else
                    t.StartPump(this);
            }

        }
        private void SystemOnOff_Click(object sender, RoutedEventArgs e)
        {
            (sender as Button).IsEnabled = false;
            if (!IsWorking)
                foreach (var pump in _Pumps.Where(pump => pump.PumpForce >= 0))
                    pump.StartPump(this);
            else
            {
                foreach (var pump in _Pumps)
                    pump.StopPump(this);
                NeedPump = false;
            }
            IsWorking = !IsWorking;
            (sender as Button).IsEnabled = true;
        }

        public event PropertyChangedEventHandler? PropertyChanged;
        public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        private void Window_Closed(object sender, EventArgs e)
        {
            foreach (var pump in _Pumps)
                pump.StopPump(this);
        }
    }
}
